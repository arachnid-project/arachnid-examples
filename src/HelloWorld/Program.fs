﻿open Arachnid.Core
open Arachnid.Machines.Http
open Arachnid.Routers.Uri.Template

(* Code

   A very simple Arachnid example! Two simple functions to see whether the client
   has supplied a name, and if so to use it in place of "World" in the classic
   greeting, a machine to deal with all of the complexities of HTTP, and a
   router to make sure suitable requests end up in the right place. *)

let name =
    arachnid {
        let! name = Arachnid.Optic.get (Route.atom_ "name")

        match name with
        | Some name -> return name
        | _ -> return "World" }

let hello =
    arachnid {
        let! name = name

        return Represent.text (sprintf "Hello %s!" name) }

let machine =
    arachnidMachine {
        handleOk hello }

let router =
    arachnidRouter {
        resource "/hello{/name}" machine }

(* Main

   A very simple program, simply a console app, with a blocking read from
   the console to keep our server from shutting down immediately. Though
   we are self hosting here as a console application, the same application
   should be easily transferrable to any OWIN compatible server, including
   IIS. *)

open KestrelInterop

[<EntryPoint>]
let main _ =

    WebHost.create ()
      |> WebHost.bindTo [|"http://0.0.0.0:5000"|]
      |> WebHost.configure (ApplicationBuilder.useArachnid (router))
      |> WebHost.buildAndRun

    0
